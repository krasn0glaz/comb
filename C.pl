#!/usr/bin/env perl
use 5.10.0;
use strict;

my $n = $ARGV[0];
my $k = $ARGV[1];

my $C = 1;
my $i = 1;

$C *= ++$i while $i < $n;
my $up = $C;

$C = 1; $i = 1; $n = $n - $k;
$C *= ++$i while $i < $n;
my $down = $C;

$C = 1; $i = 1;
$C *= ++$i while $i < $k;
$down *= $C;

my $res = $up / $down;
my $resP = 1/$res;
say "number of different occasions is \"$res\"";
say "probability is \"$up/$down\" or $resP";
